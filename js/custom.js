
/* Set the width of the side navigation to 250px */
function openNav() {
  document.getElementById("mySidenav").style.width = "100%";
  //$('#nav-res').show().fadeIn('slow');
  document.getElementById("nav-res").style.opacity = "1";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
document.getElementById("nav-res").style.opacity = "0";
//$('#nav-res').hide().fadeIn('slow');
} 

/* Set the width of the side navigation to 250px */
function openlogout() {
  document.getElementById("mySidenav-logout").style.width = "100%";
  //$('#nav-res').show().fadeIn('slow');
  document.getElementById("nav-res").style.opacity = "1";
}

/* Set the width of the side navigation to 0 */
function closelogout() {
  document.getElementById("mySidenav-logout").style.width = "0";
  document.getElementById("nav-res").style.opacity = "0";
  document.getElementById("mySidenav").style.width = "0";
  //$('#nav-res').hide().fadeIn('slow');
} 

$(window).scroll(function(){
  if ($(window).scrollTop() >= 100) {
    $('.header-top').addClass('fixed-header');
  }
  else {
    $('.header-top').removeClass('fixed-header');
  }
});

$(function() {
  $(".expand").on( "click", function() {
    // $(this).next().slideToggle(200);
    $expand = $(this).find(">:first-child");
    
    if($expand.text() == "+") {
      $expand.text("-");
    } else {
      $expand.text("+");
    }
  });
});

/*
var mediaElements = document.querySelectorAll('video, audio');

for (var i = 0, total = mediaElements.length; i < total; i++) {
  new MediaElementPlayer(mediaElements[i], {
    features: ['playpause', 'current', 'progress', 'duration', 'volume', 'quality', 'fullscreen'],
  });
}
*/


$(".js-video-button").modalVideo({
  youtube:{
    controls:0,
    nocookie: true
  }
});
